'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app
  router.get('/', controller.home.index)
  router.post('/graph/account/login', controller.user.login)
  router.post('/graph/account/register', controller.user.register)
  router.get('/graph/data', controller.graph.data),
  router.get('/graph/search/query', controller.search.data)
};
