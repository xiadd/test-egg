module.exports = {
  get isIOS() {
    const iosReg = /iphone|ipad|ipod/i;
    return iosReg.test(this.get('user-agent'));
  },

  get isAndroid() {
    return false;
  },

  get isPC() {

  },

  get isMobile() {

  },

  set name(value) {
    this.name = value + 1;
  },
};
