const Controller = require('egg').Controller;

const { graphData } = require('../../mock/graph')

class GraphController extends Controller {
  async data(ctx) {
    ctx.body = graphData
  }
}

module.exports = GraphController
