const Controller = require('egg').Controller;

const { searchData } = require('../../mock/search')

class CompanyController extends Controller {
  async data(ctx) {
    ctx.body = searchData
  }
}

module.exports = CompanyController
