'use strict';

const Controller = require('egg').Controller;

var json = [
        {
            "id": 935,
            "companyName": "中国云网有限公司",
            "childrenCompanys": [],
            "stakeInCompanys": [],
            "managers": []
        },
        {
            "id": 924,
            "companyName": "中国中信集团有限公司",
            "childrenCompanys": [
                {
                    "id": 835,
                    "startNode": 924,
                    "endNode": {
                        "id": 372,
                        "companyName": "中信证券股份有限公司",
                        "childrenCompanys": [
                            {
                                "id": 839,
                                "startNode": 372,
                                "endNode": {
                                    "id": 928,
                                    "companyName": "中信证券投资有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            },
                            {
                                "id": 838,
                                "startNode": 372,
                                "endNode": {
                                    "id": 927,
                                    "companyName": "华夏基金管理有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            },
                            {
                                "id": 837,
                                "startNode": 372,
                                "endNode": {
                                    "id": 605,
                                    "companyName": "中信期货有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            }
                        ],
                        "stakeInCompanys": [
                            {
                                "id": 824,
                                "percente": "10.2%",
                                "startNode": 372,
                                "endNode": {
                                    "id": 940,
                                    "companyName": "陕西金融资产管理股份有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            },
                            {
                                "id": 825,
                                "percente": "13.3%",
                                "startNode": 372,
                                "endNode": {
                                    "id": 941,
                                    "companyName": "国新国控（杭州）投资管理有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            }
                        ],
                        "managers": [
                            {
                                "id": 819,
                                "role": "监事会主席",
                                "startNode": {
                                    "id": 953,
                                    "name": "李放",
                                    "sex": null,
                                    "age": null,
                                    "managers": [
                                        819
                                    ],
                                    "idNumber": null
                                },
                                "endNode": 372
                            },
                            {
                                "id": 817,
                                "role": "董事长",
                                "startNode": {
                                    "id": 951,
                                    "name": "张佑君",
                                    "sex": null,
                                    "age": null,
                                    "managers": [
                                        817
                                    ],
                                    "idNumber": null
                                },
                                "endNode": 372
                            },
                            {
                                "id": 818,
                                "role": "首席风险官",
                                "startNode": {
                                    "id": 952,
                                    "name": "蔡坚",
                                    "sex": null,
                                    "age": null,
                                    "managers": [
                                        818
                                    ],
                                    "idNumber": null
                                },
                                "endNode": 372
                            }
                        ]
                    }
                },
                {
                    "id": 834,
                    "startNode": 924,
                    "endNode": {
                        "id": 925,
                        "companyName": "中信银行股份有限公司",
                        "childrenCompanys": [],
                        "stakeInCompanys": [],
                        "managers": []
                    }
                },
                {
                    "id": 836,
                    "startNode": 924,
                    "endNode": {
                        "id": 926,
                        "companyName": "中信信托有限公司",
                        "childrenCompanys": [
                            {
                                "id": 843,
                                "startNode": 926,
                                "endNode": {
                                    "id": 932,
                                    "companyName": "中信信惠国际资本有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            },
                            {
                                "id": 842,
                                "startNode": 926,
                                "endNode": {
                                    "id": 931,
                                    "companyName": "中信聚信（北京）资本管理有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            },
                            {
                                "id": 840,
                                "startNode": 926,
                                "endNode": {
                                    "id": 929,
                                    "companyName": "信城基金管理有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            },
                            {
                                "id": 841,
                                "startNode": 926,
                                "endNode": {
                                    "id": 930,
                                    "companyName": "中信信城资产管理有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            }
                        ],
                        "stakeInCompanys": [
                            {
                                "id": 833,
                                "percente": "1.64%",
                                "startNode": 926,
                                "endNode": {
                                    "id": 944,
                                    "companyName": "航天航工投资基金管理（北京）有限公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            },
                            {
                                "id": 832,
                                "percente": "1.64%",
                                "startNode": 926,
                                "endNode": {
                                    "id": 943,
                                    "companyName": "天津金融资产交易所有限责任公司",
                                    "childrenCompanys": [],
                                    "stakeInCompanys": [],
                                    "managers": []
                                }
                            }
                        ],
                        "managers": [
                            {
                                "id": 814,
                                "role": "董事长",
                                "startNode": {
                                    "id": 948,
                                    "name": "陈一松",
                                    "sex": null,
                                    "age": null,
                                    "managers": [
                                        814
                                    ],
                                    "idNumber": null
                                },
                                "endNode": 926
                            },
                            {
                                "id": 816,
                                "role": "副董事长",
                                "startNode": {
                                    "id": 950,
                                    "name": "张翔燕",
                                    "sex": null,
                                    "age": null,
                                    "managers": [
                                        816
                                    ],
                                    "idNumber": null
                                },
                                "endNode": 926
                            },
                            {
                                "id": 815,
                                "role": "副董事长",
                                "startNode": {
                                    "id": 949,
                                    "name": "路京生",
                                    "sex": null,
                                    "age": null,
                                    "managers": [
                                        815
                                    ],
                                    "idNumber": null
                                },
                                "endNode": 926
                            }
                        ]
                    }
                }
            ],
            "stakeInCompanys": [
                {
                    "id": 823,
                    "percente": "100%",
                    "startNode": 924,
                    "endNode": {
                        "id": 934,
                        "companyName": "中信资产管理有限公司",
                        "childrenCompanys": [],
                        "stakeInCompanys": [],
                        "managers": []
                    }
                },
                {
                    "id": 821,
                    "percente": "100%",
                    "startNode": 924,
                    "endNode": 935
                },
                {
                    "id": 822,
                    "percente": "75%",
                    "startNode": 924,
                    "endNode": {
                        "id": 933,
                        "companyName": "中信正业控股有限公司",
                        "childrenCompanys": [],
                        "stakeInCompanys": [],
                        "managers": []
                    }
                }
            ],
            "managers": [
                {
                    "id": 813,
                    "role": "纪委书记",
                    "startNode": {
                        "id": 947,
                        "name": "冯光",
                        "sex": null,
                        "age": null,
                        "managers": [
                            813
                        ],
                        "idNumber": null
                    },
                    "endNode": 924
                },
                {
                    "id": 812,
                    "role": "总经理",
                    "startNode": {
                        "id": 946,
                        "name": "王炯",
                        "sex": null,
                        "age": null,
                        "managers": [
                            812
                        ],
                        "idNumber": null
                    },
                    "endNode": 924
                },
                {
                    "id": 811,
                    "role": "董事长",
                    "startNode": {
                        "id": 945,
                        "name": "常振明",
                        "sex": null,
                        "age": null,
                        "managers": [
                            811
                        ],
                        "idNumber": null
                    },
                    "endNode": 924
                }
            ]
        },
        {
            "id": 923,
            "companyName": "国务院",
            "childrenCompanys": [],
            "stakeInCompanys": [
                {
                    "id": 820,
                    "percente": "100%",
                    "startNode": 923,
                    "endNode": 924
                }
            ],
            "managers": []
        },
        934,
        933,
        946,
        947,
        945,
        925,
        372,
        951,
        953,
        952,
        {
            "id": 937,
            "companyName": "中央汇金资产管理有限公司",
            "childrenCompanys": [],
            "stakeInCompanys": [
                {
                    "id": 829,
                    "percente": "1.64%",
                    "startNode": 937,
                    "endNode": 372
                }
            ],
            "managers": []
        },
        {
            "id": 938,
            "companyName": "中国证券金融股份有限公司",
            "childrenCompanys": [],
            "stakeInCompanys": [
                {
                    "id": 828,
                    "percente": "4.83%",
                    "startNode": 938,
                    "endNode": 372
                }
            ],
            "managers": []
        },
        941,
        940,
        {
            "id": 936,
            "companyName": "中国中信有限公司",
            "childrenCompanys": [],
            "stakeInCompanys": [
                {
                    "id": 830,
                    "percente": "1.64%",
                    "startNode": 936,
                    "endNode": 926
                },
                {
                    "id": 827,
                    "percente": "16.54%",
                    "startNode": 936,
                    "endNode": 372
                }
            ],
            "managers": []
        },
        926,
        950,
        {
            "id": 942,
            "companyName": "中信兴业投资集团有限公司",
            "childrenCompanys": [],
            "stakeInCompanys": [
                {
                    "id": 831,
                    "percente": "18%",
                    "startNode": 942,
                    "endNode": 926
                }
            ],
            "managers": []
        },
        948,
        949,
        943,
        944,
        931,
        932,
        929,
        930,
        {
            "id": 939,
            "companyName": "香港中央结算（代理人）有限公司",
            "childrenCompanys": [],
            "stakeInCompanys": [
                {
                    "id": 826,
                    "percente": "18.79%",
                    "startNode": 939,
                    "endNode": 372
                }
            ],
            "managers": []
        },
        927,
        928,
        605
]

class HomeController extends Controller {
  async index (ctx) {
    await ctx.render('index.njk');
  }

  async mock (ctx) {
    ctx.body = json
  }
}

module.exports = HomeController;
