const Controller = require('egg').Controller;

const { loginInfo, registerInfo } = require('../../mock/user')

class UserController extends Controller {

  /**
   * 用户登录mock
   * @param {*} ctx
   */
  async login(ctx) {
    ctx.body = loginInfo
  }

  async register(ctx) {
    ctx.body = registerInfo
  }
}

module.exports = UserController
