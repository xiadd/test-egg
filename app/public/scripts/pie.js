void function createD3Pie (d3) {
  const svg = d3.select('#pie')
  const width = 1140
  const height = 360
  const radius = Math.min(width, height) / 2
  const donutWidth = 75
  const color = d3.scaleOrdinal(d3.schemeCategory20b)

  const dataset = [
    { label: 'Abulia', count: 10 },
    { label: 'Betelgeuse', count: 20 },
    { label: 'Cantaloupe', count: 30 },
    { label: 'Dijkstra', count: 40 }
  ]

  const g = svg
    .append('g')
    .attr('transform', `translate(${width / 2}, ${height / 2}) scale(0.5)`)

  const arc = d3.arc()
    .innerRadius(radius - donutWidth)             // NEW
    .outerRadius(radius)

  const pie = d3.pie()
    .value(d => d.count)
    .sort(null)

  const path = g.selectAll('path')
    .data(pie(dataset))
    .enter()
    .append('path')
    .attr('d', arc)
    .attr('fill', d => color(d.data.label))
    .on('click', function (d, i) {
      console.log(d)
      d3.select(this)
        .transition()
        .attr('transform', `translate(10, 10)`)
    })
} (d3)
