exports.searchData = {
    "msg": "",
    "data": {
        "companyList": {
            "content": [
                {
                    "id": 370,
                    "companyName": "中信建投证券股份有限公司",
                    "childrenCompanys": [],
                    "stakeInCompanys": [],
                    "managers": []
                },
                {
                    "id": 371,
                    "companyName": "中信证券（山东）有限责任公司",
                    "childrenCompanys": [],
                    "stakeInCompanys": [],
                    "managers": []
                },
                {
                    "id": 372,
                    "companyName": "中信证券股份有限公司",
                    "childrenCompanys": [],
                    "stakeInCompanys": [],
                    "managers": []
                },
                {
                    "id": 588,
                    "companyName": "中信信诚资产管理有限公司",
                    "childrenCompanys": [],
                    "stakeInCompanys": [],
                    "managers": []
                },
                {
                    "id": 605,
                    "companyName": "中信期货有限公司",
                    "childrenCompanys": [],
                    "stakeInCompanys": [],
                    "managers": []
                }
            ],
            "totalPages": 4,
            "totalElements": 20,
            "last": false,
            "size": 5,
            "number": 0,
            "sort": null,
            "first": true,
            "numberOfElements": 5
        },
        "managerList": {
            "content": [],
            "totalPages": 0,
            "totalElements": 0,
            "last": true,
            "size": 5,
            "number": 0,
            "sort": null,
            "first": true,
            "numberOfElements": 0
        }
    },
    "success": true,
    "status": 0
}