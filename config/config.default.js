'use strict';
const path = require('path')

module.exports = appInfo => {
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1518312526975_4315';

  // add your config here
  config.middleware = [];

  // 添加 view 配置
  exports.view = {
    defaultViewEngine: 'nunjucks',
    mapping: {
      '.njk': 'nunjucks',
    },
  };

  exports.security = {
    csrf: {
      enable: false,
    }
  }

  exports.cors = {
    origin: '*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
  };

  exports.jwt = {
    secret: '12345',
    enable: true, // default is false
    match: '/api'
  }

  exports.static = {
    prefix: '/',
    dir: path.join(appInfo.baseDir, 'app/public'),
    dynamic: true
    // maxAge: 31536000,
  }

  return config;
};
